var https = require('https');
var parseString = require('xml2js').parseString;
var rp = require('request-promise');
var Promise = require('promise');
var dateMath = require('date-arithmetic');


/**
* this call will get the list of Communities from Connections
* @param {object} json containing credentials
* @returns {object} json containing the data from Connections
*/
exports.getAllCommunities = function(properties) {
  
  return new Promise(function(resolve, reject) {
    
    var ALL_COMMUNITIES_URI = '/communities/service/atom/communities/all?ps=500';

    var options = {
        method: 'GET',
        uri: 'https://' + properties.get('connections_host') + ALL_COMMUNITIES_URI,
        "auth": {
          "user": properties.get('connections_userid'),
          "pass": properties.get('connections_password')
      },
      json: false // do not parse the result to JSON
    };
    rp(options).then(function (resultXML) {
      // parse the XML to JSON
      parseString(resultXML, { explicitArray:false }, function(err, parsedXml) {
        if ( err === null ) {
          // communityInfo is the array of data that will be passed back.
          var communityInfo = [];
          // communityData is and object that stores all of the community information keyed on the community id.
          // This structure exists so the getCommunityFiles call can updated the correct object to be returned.
          var communityData = {};

          // this is a brute force method to resolve the promise only when all communities are filled in.
          var completedFiles = 0;
          var totalCalls = parsedXml.feed.entry.length;
          for (var i = 0; i < parsedXml.feed.entry.length; i++) {
            var communityId = parsedXml.feed.entry[i]['snx:communityUuid'];

            // save the community information using the communityId
            communityData[communityId] = {};
            // save off the needed information
            communityData[communityId]["id"] = communityId;
            communityData[communityId]["title"] = parsedXml.feed.entry[i].title._;
            communityData[communityId]["updated"] = parsedXml.feed.entry[i].updated;
            communityData[communityId]["creator"] = parsedXml.feed.entry[i].author.name;
            communityData[communityId]["created"] = parsedXml.feed.entry[i].published;
            communityData[communityId]["membercount"] = parsedXml.feed.entry[i]["snx:membercount"];
            exports.getCommunityFiles(properties, communityId).then(function (fileResults){ 
              //javascript module that has all callbacks
              
              // default the file size to 0
              var fileSize = 0;
              if (fileResults.totalFileSize) {
                // if the totalFileSize was returned, save it off
                fileSize = fileResults.totalFileSize;
              }
              
              // save the fileSize
              communityData[fileResults.id]["fileSize"] = fileSize;
              // add the entry to the communityInfo
              communityInfo.push(communityData[fileResults.id]);
        
              // check to see if we completed all of the calls
              completedFiles++;
              if (completedFiles >= totalCalls) {
                resolve(communityInfo);
              }
            }).catch(function(errorMessage) {
              // increment the counter to make sure the app doesn't hang
              completedFiles++;
            });
          }
        } else {
          // handle error condition in parse
          console.log('error parsing getting all communities!!', err);
          reject('error parsing all communities: ' + err.message);  
        }
      });
    })
    .catch(function (err) {
      // handle error condition in request
      console.log('error getting all communities:', err);
      reject('error getting all communities: ' + err.message);
    });
  });
};



exports.getDetails = function(properties, communities, community) {
  
  return new Promise( (resolve, reject) => {
    
  //  console.log('in getDetails; this many communities:', communities.length);
    var promises = [];
    for ( var k = 0; k < communities.length; k++) {
      //console.log('processing community ', communities[k].id)
      
        promises.push(community.getCommunityMembers(properties, communities[k].id));  
      promises.push(community.getCommunityFiles(properties, communities[k].id));
      
    }
    
    
    Promise.all(promises).then(function(allData) { // this will run when all promises have returned
     resolve(allData);
    }, function(err) {
      console.log('oops, an error:', err.statusCode, err.statusMessage);
      var errMsg = {action: "get community details", statusCode: err.statusCode, statusMessage: err.statusMessage};
      reject(errMsg);
    });
  });
};


/**
 * Get the list of members of a community
 * @param {object} json containing credentials
 * @param {string} community Uuid
 * @returns {Promise} a Promise which resolves to json containing the member list
*/
exports.getCommunityMembers = function(properties, id) {

  console.log('in .getCommunityMembers, id is [', id, ']');

  return new Promise(function(resolve, reject){
    
    var COMM_MEMBERS_URI = '/communities/service/atom/community/members?ps=1000&communityUuid=';

    var options = {
        method: 'GET',
        uri: 'https://' + properties.get('connections_host') + COMM_MEMBERS_URI + id,     
        "auth": {
          "user": properties.get('connections_userid'),
          "pass": properties.get('connections_password')
      },
      json: false // do not parse the result to JSON
    };
    //console.log('URI for members is [', 'https://' + properties.get('connections_host') + COMM_MEMBERS_URI + id, ']');
  
    rp(options)
    .then(function (resultXML) {
      // set explicitArray to true to force an array so we can iterate through it, even if there is only one result
      parseString(resultXML, { explicitArray:true }, function(err, parsedXml) {
        if ( err === null ) {
          var members = [];
          for (var i = 0; i < parsedXml.feed.entry.length; i++) {
            // if a user is inactive, there is no email address...so check for one
            // we will only want to grab owners which is flagged in x-community-role
            var member = {
                name: parsedXml.feed.entry[i].contributor[0].name[0],
                communityid: id,
                email: parsedXml.feed.entry[i].contributor[0].email ? parsedXml.feed.entry[i].contributor[0].email[0] : '',           
                state: parsedXml.feed.entry[i].contributor[0]["snx:userState"][0]._
            };          
            var navigateToOwner = parsedXml.feed.entry[i].content[0].div[0].span[0]; //find the section of xml output which contains x-community-role         

            var objToStr = JSON.stringify(navigateToOwner);  //what is returned is multiple objects, can't navigate further down as there is no key for what we need
            var n=(objToStr.indexOf('"owner"'));          
            if (n > -1) {
              members.push(member);  //we only care about owners
              //console.log(member);
            }
          }
          resolve({"type":"members", "data": members});
        
        } else {
          console.log('error parsing members:', err);
          reject('error parsing members: ' + err.message);
        }
      });
    })
    .catch(function(err){
      console.log('error getting community members', err.message);
      console.log('the error is for the community id ', id);
      reject('error getting community members: ' + err.message);
    });
  });


};


/**
 * Get the list of files of a community
 * @param {object} json containing credentials
 * @param {string} community Uuid
 * @returns {Promise} a Promise which resolves to json containing the files list
*/
exports.getCommunityFiles = function(properties, id){

  //console.log('in .getCommunityFiles, id is [', id, ']');

  return new Promise(function(resolve, reject){
    
    var COMM_FILES_URI = '/files/basic/api/communitycollection/'
      + id
      + '/feed?sC=document&pageSize=500&sortBy=title&type=communityFiles';

    var options = {
        method: 'GET',
        uri: 'https://' + properties.get('connections_host') + COMM_FILES_URI,
        "auth": {
          "user": properties.get('connections_userid'),
          "pass": properties.get('connections_password')
      },
      json: false // don't parse the result to JSON
    };
  //  console.log('before rp');
    rp(options)   
    .then(function (result) {
  
      parseString(result, { explicitArray:true }, function(err, parsedXml) {
        if ( err === null ) {
          var files = [];
        var totalCommunityFileSize =0;
          if ( parsedXml.feed['opensearch:totalResults'][0] > 0 ) {
            for (var i = 0; i < parsedXml.feed.entry.length; i++) {
    
              var sizeLink = parsedXml.feed.entry[i].link.find(function(item) {
                return typeof item.$.length !== 'undefined';
              });
    
              totalCommunityFileSize += sizeLink.$.length*1;  // we don't care about individual files, just the grand total
            }
            var file = {
                //id: id,
                size: totalCommunityFileSize
            };    
            files.push(file);
          }
          resolve({"type":"filesize", "totalFileSize": totalCommunityFileSize, "id": id});
        } else {
          console.log('error parsing files:', err);
          reject('error parsing files: ' + err.message);
        }
      });
    })
    .catch(function(err){
      console.log('error getting community files', err);
      reject(err);
    });
  });
};

/**
 * Get the list of subcommunities of a community
 * @param {object} json containing credentials
 * @param {string} community Uuid
 * @returns {Promise} a Promise which resolves to json containing the subcommunities list
*/
exports.getSubcommunities = function(properties, id){

  console.log('in .getSubcommunities, id is [', id, ']');

  return new Promise(function(resolve, reject){
    
    var SUBCOMMUNITIES_URI = '/communities/service/atom/community/subcommunities?communityUuid='
      + id;

    var options = {
        method: 'GET',
        uri: 'https://' + properties.get('connections_host') + SUBCOMMUNITIES_URI,
        "auth": {
          "user": properties.get('connections_userid'),
          "pass": properties.get('connections_password')
      },
      json: false // don't parse the result to JSON
    };

    rp(options)
    .then(function (result) {
      parseString(result, { explicitArray:true }, function(err, parsedXml) {
        if ( err === null ) {
          var subcommunities = [];
          if ( parsedXml.feed['opensearch:totalResults'][0] > 0 ) {
            for (var i = 0; i < parsedXml.feed.entry.length; i++) {
              var subcommunity = {
                  title: parsedXml.feed.entry[i].title[0]._
              };
              subcommunities.push(subcommunity);
            }
          }
          resolve({"type":"subcommunities", "data": subcommunities});
        } else {
          console.log('error parsing subcommunities:', err);
          reject('error parsing subcommunities: ' + err.message);
        }
      });
    })
    .catch(function(err){
      console.log('error getting subcommunities', err);
      reject(err);
    });
  });
};


/**
 * Get the list of recent activity (last 30 days) of a community
 * @param {object} json containing credentials
 * @param {string} community Uuid
 * @returns {Promise} a Promise which resolves to json containing the activity list
*/
exports.getRecentActivity = function(properties, id){
  
  console.log('in .getRecentActivity, id is [', id, ']');

  return new Promise(function(resolve, reject){
    
    var oneMonthAgo = dateMath.subtract(new Date(), 30, 'day').toISOString();
    
    var COMM_ACTIVITY_URI = '/connections/opensocial/basic/rest/activitystreams/urn:lsid:lconn.ibm.com:communities.community:'
      + id 
      + '/@all/@all?rollup=true&shortStrings=true&format=json&updatedSince' + oneMonthAgo;
      
    var options = {
        method: 'GET',
        uri: 'https://' + properties.get('connections_host') + COMM_ACTIVITY_URI,
        "auth": {
          "user": properties.get('connections_userid'),
          "pass": properties.get('connections_password')
      },
      json: true // parse the body to JSON!
    };

    rp(options)
    .then(function (result) {
        var activity = [];
        for (var i = 0; i < result.list.length; i++) {
          var details = {
              name : result.list[i].connections.containerName,
              title: result.list[i].connections.plainTitle,
              author: result.list[i].actor.displayName,
              publishedDate: result.list[i].published,
              shortTitle: result.list[i].connections.shortTitle,
              itemUrl: result.list[i].openSocial.embed.context.itemUrl
            };
            activity.push(details);
        }
        resolve({"type":"activity", "data": activity});

    })
    .catch(function(err){
      console.log('error getting recent activity', err);
      reject(err);
    });
  });
};
